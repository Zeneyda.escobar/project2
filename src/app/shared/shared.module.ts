import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MantenimientosIconComponent } from './icons/mantenimientos-icon/mantenimientos-icon.component';
import { RegistrosIconComponent } from './icons/registros-icon/registros-icon.component';
import { ProcesosIconComponent } from './icons/procesos-icon/procesos-icon.component';
import { ReportesIconComponent } from './icons/reportes-icon/reportes-icon.component';
import { HistoricosIconComponent } from './icons/historicos-icon/historicos-icon.component';
import { OtrosIconComponent } from './icons/otros-icon/otros-icon.component';
import { EstadisticasIconComponent } from './icons/estadisticas-icon/estadisticas-icon.component';
import { ProveedoresIconComponent } from './icons/proveedores-icon/proveedores-icon.component';
import { ItemsIconComponent } from './icons/items-icon/items-icon.component';
import { EspecificacionesIconComponent } from './icons/especificaciones-icon/especificaciones-icon.component';



@NgModule({
  declarations: [
    MantenimientosIconComponent,
    RegistrosIconComponent,
    ProcesosIconComponent,
    ReportesIconComponent,
    HistoricosIconComponent,
    OtrosIconComponent,
    EstadisticasIconComponent,
    ProveedoresIconComponent,
    ItemsIconComponent,
    EspecificacionesIconComponent
  ],
  imports: [
    CommonModule,
  ],
  exports:[
    MantenimientosIconComponent,
    RegistrosIconComponent,
    ProcesosIconComponent,
    ReportesIconComponent,
    HistoricosIconComponent,
    OtrosIconComponent,
    EstadisticasIconComponent,
    ProveedoresIconComponent,
    ItemsIconComponent,
    EspecificacionesIconComponent
  ]
})
export class SharedModule { }
