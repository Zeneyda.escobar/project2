import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { MantenimientoComponent } from './pages/mantenimiento/mantenimiento.component';
import { MantenimientosRoutingModule } from './mantenimientos-routing.module';


@NgModule({
  declarations: [
    MantenimientoComponent
  ],
  imports: [
    CommonModule,
    MantenimientosRoutingModule,
    SharedModule
  ]
})
export class MantenimientosModule { }
