import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { MantenimientosIconComponent } from '../../shared/icons/mantenimientos-icon/mantenimientos-icon.component';
import { RegistrosIconComponent } from '../../shared/icons/registros-icon/registros-icon.component';
import { ProcesosIconComponent } from '../../shared/icons/procesos-icon/procesos-icon.component';
import { ReportesIconComponent } from '../../shared/icons/reportes-icon/reportes-icon.component';
import { HistoricosIconComponent } from '../../shared/icons/historicos-icon/historicos-icon.component';
import { OtrosIconComponent } from '../../shared/icons/otros-icon/otros-icon.component';
import { EstadisticasIconComponent } from '../../shared/icons/estadisticas-icon/estadisticas-icon.component';
import { ProveedoresIconComponent } from '../../shared/icons/proveedores-icon/proveedores-icon.component';
import { ItemsIconComponent } from '../../shared/icons/items-icon/items-icon.component';
import { EspecificacionesIconComponent } from '../../shared/icons/especificaciones-icon/especificaciones-icon.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule { }
