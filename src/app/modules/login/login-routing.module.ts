import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RestablecerComponent } from './pages/restablecer/restablecer.component';
import { AccesoComponent } from './pages/acceso/acceso.component';

const routes: Routes = [
  {
    path:'',
    component:LoginComponent,
    title:'SIEM'
  },
  {
    path:'restablecer',
    component: RestablecerComponent,
    title:'Restablecer'
  },{
    path:'acceso',
    component:AccesoComponent,
    title:'SIEM'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
