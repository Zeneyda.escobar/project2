import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { CardLoginComponent } from './components/card-login/card-login.component';
import { CardRestablecerComponent } from './components/card-restablecer/card-restablecer.component';
import { RestablecerComponent } from './pages/restablecer/restablecer.component';
import { AccesoComponent } from './pages/acceso/acceso.component';


@NgModule({
  declarations: [
    LoginComponent,
    CardLoginComponent,
    CardRestablecerComponent,
    RestablecerComponent,
    AccesoComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule
  ]
})
export class LoginModule { }
